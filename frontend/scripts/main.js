import offeringsService from './offeringsService.js';

function createOfferingsTable() {
  // Hilfsfunktion: Zeile in Preistabelle einfügen und
  // Dropdown für Preis Update anpassen
  function addRowToTable(offering) {
    const elRow = createTBodyRow(offering);
    elTBody.appendChild(elRow);
    updateVendorDropdown(offering);
  }

  // Hilfsfunktion: Zeile für Preistabelle erzeugen
  function createTBodyRow(offering) {
    // Element für Zeile erzeugen
    let elRow = document.createElement('tr');
    // Element für Zelle mit Anbieter erzeugen und Anbieter hinzufügen
    let elCellVendor = document.createElement('td');
    elCellVendor.textContent = offering.vendor;
    // Zelle mit Anbieter Zeile hinzufügen
    elRow.appendChild(elCellVendor);
    // Element für Zelle mit Preis erzeugen und Preis hinzufügen
    let elCellPrice = document.createElement('td');
    elCellPrice.textContent = offering.price;
    // Zelle mit Anbieter Zeile hinzufügen
    elRow.appendChild(elCellPrice);
    // Element für Button zum Löschen erzeugen und hinzufügen
    let elDelOffering = document.createElement('td');
    let elDelBtn = document.createElement('button');
    elDelBtn.textContent = 'Löschen';
    // Event-Listener mit Callback Methode zum Löschen
    elDelBtn.addEventListener('click', () => {
      // Angebot aus Backend löschen
      offeringsService.remove(offering.id).then(() => {
        // Angebot aus Frontend löschen
        offerings.splice(offerings.indexOf(offering), 1);
        // Preistabelle neu erstellen
        createOfferingsTable();
      });
    });
    elDelOffering.appendChild(elDelBtn);
    // Zelle mit Button hinzufügen
    elRow.appendChild(elDelOffering);
    return elRow;
  }

  // Hilfsfunktion: Dropdown für Preis Update anpassen
  function updateVendorDropdown() {
    // Dropdown selektieren
    const elDropdown = document.getElementById('select-vendor');
    // Bisherige Einträge löschen
    elDropdown.innerHTML = '';
    // Für alle Offerings option-Element hinzufügen
    for (let i = 0; i < offerings.length; i++) {
      const offering = offerings[i];
      // Neues option-Element erstellen
      const elOp = document.createElement('option');
      elOp.setAttribute('value', offering.id);
      elOp.textContent = offering.vendor;
      // option-Element Dropdown hinzufügen
      elDropdown.appendChild(elOp);
    }
  }

  // Bisherige Einträge in Preistabelle löschen
  const elTBody = document.querySelector('#price-table tbody');
  elTBody.innerHTML = '';
  // Angebote nach Preis sortieren: Kleinster Preis zuerst
  offerings.sort((o1, o2) => {
    if (o1.price < o2.price) {
      return -1;
    }
    if (o1.price > o2.price) {
      return 1;
    }
    return 0;
  });
  // Alle Angebote Preistabelle hinzufügen
  for (let i = 0; i < offerings.length; i++) {
    const offering = offerings[i];
    addRowToTable(offering);
  }
}

function addOffering() {
  // Eingabe für Anbieter und Preis auslesen
  const elVendorInput = document.getElementById('vendor');
  const vendor = elVendorInput.value;
  const elPriceInput = document.getElementById('price');
  const price = parseInt(elPriceInput.value);
  // Wurde sinnvolle Eingabe gemacht?
  if (vendor && price) {
    // Angebot Backend hinzufügen
    offeringsService
      .create({
        vendor: vendor,
        price: price,
      })
      .then((newOffering) => {
        // Neues Angebot Frontend hinzufügen
        offerings.push(newOffering);
        // Preistabelle neu erstellen
        createOfferingsTable();
      });
  }
  // Eingabefelder löschen
  elVendorInput.value = '';
  elPriceInput.value = '';
}

function changePrice() {
  // Eingabe für Anbieter und neuer Preis auslesen
  const elDropdown = document.getElementById('select-vendor');
  const idOffering = elDropdown.value;
  const idxVendor = elDropdown.selectedIndex;
  const elNewPrice = document.getElementById('new-price');
  const newPrice = parseInt(elNewPrice.value);
  // Wurde sinnvoller eingegeben?
  if (newPrice) {
    // Angebot suchen
    let toUpdate = offerings.filter((o) => o.id === idOffering)[0];
    // Angebot im Backend anpassen
    offeringsService
      .update(idOffering, {
        vendor: toUpdate.vendor,
        price: newPrice,
      })
      .then(() => {
        // Angebot im Frontend anpassen
        toUpdate.price = newPrice;
        // Eintrag in Preistabelle anpassen
        const elTdToUpdate = document.querySelector(
          '#price-table tbody tr:nth-child(' +
            (idxVendor + 1) +
            ') td:nth-child(2)'
        );
        elTdToUpdate.textContent = newPrice;
        // Preistabelle neu erstellen
        createOfferingsTable();
      });
  }
  elNewPrice.value = '';
}

let offerings = [];
// Am Anfang alle Angebote laden und Preisliste
// hinzfuügen
offeringsService.getAll().then((data) => {
  // Geladene Angebote in Frontend in Array speichern
  for (let i = 0; i < data.length; i++) {
    const offering = data[i];
    offerings.push(offering);
  }
  createOfferingsTable();
});

// Button "Hinzufügen" Event-Listener hinzufügen
const elBtnAdd = document.getElementById('add-offering');
elBtnAdd.addEventListener('click', addOffering);

// Button "Ändern" Event-Listener hinzufügen
const elBtnChange = document.getElementById('change-price');
elBtnChange.addEventListener('click', changePrice);
